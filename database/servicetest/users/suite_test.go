package users

import (
	"testing"

	s "github.com/stretchr/testify/suite"

	"gitlab.com/quocbang/api-server-basic/database/orm/models"
	"gitlab.com/quocbang/api-server-basic/database/servicetest/internal/suite"
)

type Suite struct {
	suite.SuiteConfig
}

// NewSuite create all necessary.
func NewSuite() *Suite {
	s := suite.NewSuiteTest(suite.NewSuiteParameters{
		RelativeModels:       []models.Models{&models.Users{}},
		ClearDataForEachTest: true,
	})
	return &Suite{
		SuiteConfig: *s,
	}
}
func TestSuite(t *testing.T) {
	// Run the test suite using suite.Run.
	s.Run(t, NewSuite())
}
